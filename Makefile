CC = go
CFLAGS = install
PEER  = peer
GOPATH := ${PWD}
export GOPATH

all: build

build:  clean
	$(CC) $(CFLAGS) $(PEER)

clean:
	rm -rf ./pkg/
	rm -rf ./bin/

