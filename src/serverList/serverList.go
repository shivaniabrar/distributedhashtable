package serverList

import (
	"github.com/olebedev/config"
	"fmt"
	"strconv"
	"net/rpc"
)

type Server struct{
	id string
	ip string
	port string
	conn *rpc.Client
}

type ServerList struct {
	list map[string]*Server
	totServers int
	Replicationfactor int
}

func (s *ServerList) InitServerList(config_file string) {
	
	s.totServers = 0
	s.list = make(map[string]*Server)
	
	cfg,err := config.ParseYamlFile(config_file)
	if err!= nil {
		fmt.Println(err)
		return
	}

	for {
		id, err := cfg.String("servers."+strconv.Itoa(s.totServers)+".id")
		if err != nil {
			break
		}
		ip, err := cfg.String("servers."+strconv.Itoa(s.totServers)+".ip")
		if err != nil {
			break
		}
		port, err := cfg.String("servers."+strconv.Itoa(s.totServers)+".port")
		if err != nil {
			break
		}		
		s.list[id]= &Server{id,ip,port,nil}
		s.totServers += 1
	}

	replicationfactor, err := cfg.Int("replication.0.replicas")
	if err == nil {
		s.Replicationfactor = replicationfactor	
	}
}

func (s *ServerList) CheckId(id string) (bool){
	server := s.list[id]
	if server == nil{
		return false
	}
	return true
}

func (s *ServerList) GetId(index string) (id string){
	id = s.list[index].id
	return
}

func (s *ServerList) GetIP(id string) (ip string){
	ip = s.list[id].ip
	return
}

func (s *ServerList) GetPort(id string) (port string){
	server := s.list[id]
	if server == nil {
		port = ""
		return
	}
	port = server.port		
	return
}

func (s *ServerList) GetConnection(id string) (conn *rpc.Client){
	conn = s.list[id].conn
	return
}

func (s *ServerList) MakeConnection(id string) (conn *rpc.Client,err error){
	conn,err = rpc.Dial("tcp", s.list[id].ip + ": " + s.list[id].port)
	if err!= nil {
		conn = nil
		return
	}
	s.list[id].conn = conn
	return
}

func (s *ServerList) GetTotalNumberofServers() (totServers int){
	totServers = s.totServers
	return
}