package main

import (
	"fmt"
	"hashservice"
	"utility/rpcserver"
	"serverList"
	"distributedhash"
//	"time"
	"flag"
)

var menu = []string{"Get","Put","Delete"}


func main() {


	var id string
	var path string

	id,path = getUserParamaters()	
	
	server := new(rpcserver.RpcServer)
	servers := new(serverList.ServerList)
	servers.InitServerList(path)
	status := servers.CheckId(id)
	if status == false {
		fmt.Println("Invalid id:", id)
		return
	}
	port := servers.GetPort(id)
	
	/* Init & Start Server */
	server.InitRpcServer("",port)
	hashService := new(hashservice.Service)
	hashservice.InitService(hashService)
	go server.StartRpcServer(hashService)

	/* Create and Init Dht*/
	dht := new(distributedhash.Dht)
	dht.InitDht(servers,id)

	for {
		runUI(dht)
	}

}

func getUserParamaters() (string,string) {
	id := flag.String("id", "0", "id of server")
	path := flag.String("path", "./servers.yml", "Path of file storing server list")
	flag.Parse()
	return *id,*path
}

func runUI(dht *distributedhash.Dht) {
	var choice int
	var key string
	var value string

	fmt.Println("\nWhat you would like to do?: \n")
	for index, operation := range menu {
		/* Since index starts from 0 */
		fmt.Printf("%d. %s\n", index+1, operation)
	}
	
	fmt.Print("Enter your choice: ")
	fmt.Scanf("%d", &choice)

	fmt.Print("Enter key: ")
	fmt.Scanf("%s",&key)
	
	switch {
	case choice == 1:
		val,status := (dht.Get(key))
		if val != nil {
			value = val.(string)
			fmt.Println("Value:",value)
		}else{
			if status {
				fmt.Println("Key doesn't exist")
			}
		}
	
	case choice == 2:
		fmt.Print("Enter value: ")
		fmt.Scanf("%s",&value)
		status := dht.Put(key,value)
		if status==false {
			fmt.Println("Put fail")
		}

	case choice == 3:
		dht.Delete(key)


	case choice > 3 || choice <= 0:
		fmt.Println("Invalid Input")
	}
}